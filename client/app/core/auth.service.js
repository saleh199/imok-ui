function Auth($http, $cookies, $q, $rootScope, environmentConfig)
{
    var adminId = null;
    var url = environmentConfig.url + 'admins/';

    function getAdminIdFromCookie() {
        var admin_id = $cookies.get('adminId');
        if(admin_id) {
            adminId = admin_id;
            $rootScope.adminId = admin_id;

            // For select2 directives
            window.adminId = admin_id;
        }
    }

    var login = function(email, password) {
        var deferred = $q.defer();
        $http
            .post(url + 'auth', {
                email: email,
                password: password
                })
            .then(function(response){
                adminId = response.data.result;
                if($cookies.get('adminId')) {
                    $cookies.remove('adminId');
                }
                $cookies.put('adminId', adminId);
                deferred.resolve(adminId);
            }, function(response){
                deferred.reject(response);
            });

        return deferred.promise;
    };

    var logout = function(successCallback, errorCallback){
        var deferred = $q.defer();
        $http
            .post(url + 'logout')
            .then(function(response){
                $cookies.remove('adminId');
                successCallback(response);
                deferred.resolve(response);
            }, function(response){
                errorCallback(response);
                deferred.reject(response);
            });

        return deferred.promise;
    };

    var getAdmin = function(){
        var deferred = $q.defer();
        $http
            .get(url + 'check-login')
            .then(function(successResponse){
                deferred.resolve(successResponse);
            },function(errorResponse){
                deferred.reject(errorResponse);
            });

        return deferred.promise;
    };

    var isAuthenticated = function() {
        return !!adminId;
    };

    getAdminIdFromCookie();

    return {
        login: login,
        logout: logout,
        isAuthenticated: isAuthenticated,
        getAdmin: getAdmin
    };
}

Auth.$inject = [
    '$http',
    '$cookies',
    '$q',
    '$rootScope',
    'environmentConfig'
];

angular.module('orca.core')
    .factory('Auth', Auth);