angular.module('orca.core')
    .factory('Country', [
        '$filter',
        'resource',
        'CacheFactory',
        Country
    ]);

function Country($filter, resource, CacheFactory) {
    var url = 'countries';
    var items = [];
    var itemsKeyValue = [];

    var resourceService = resource(
        url,
        {
            id: '@id'
        },
        {
            query: {
                isArray: false,
                cache: getCacheFactory()
            }
        }
    );

    function init() {
        return resourceService.query(null, initSuccess, initError);
    }

    function initSuccess(response) {
        items = response.result;
        var newItems = [];

        for(var i in items) {
            newItems.push({
                id: items[i]._id,
                name: items[i].name.common,
                currency: items[i].currency[0],
                latlng: items[i].latlng,
                callingCode: items[i].callingCode[0]
            });
        }
        items = newItems;

        return items;
    }

    function initError(response, a, b, c) {
        console.error(response, a, b, c);
    }

    function getItems() {
        return items;
    }

    function getItem(id) {
        var item = $filter('filter')(items, function(country){
            return country.id == id;
        });
        return item[0] || null;
    }

    function getCacheFactory() {
        return CacheFactory.get('countries') || CacheFactory('countries', {
                maxAge: 24*60*60*1000, // 1 day
                onExpire: cacheExpired
            });
    }

    function cacheExpired() {
        init();
    }

    return {
        getItems: getItems,
        getItem: getItem,
        init: init
    };
}