function AdminsListCtrl($scope, $rootScope, $route, dgNotifications, queryString, Admins)
{
    $scope.loading = [];
    $scope.list = [];

    $scope.loadList = function(){
        var data = angular.copy($scope.filters);

        var loading = Admins.query(
            data,
            function(successResponse){
                $scope.list = successResponse.result;
            }, function(errorResponse){
                dgNotifications.error(errorResponse);
            }
        );

        $scope.loading.push(loading);
    };

    $scope.openItemForm = function(item) {
        $scope.openModal(
            'admins/form.modal.html',
            'AdminCtrl',
            item ? item._id : null
        );
    };


    $rootScope.$on('admin:update', function(){
        $scope.loadList();
    });

    $scope.loadList();
    $scope.$root.heading.name = $route.current.label;
}

AdminsListCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$route',
    'dgNotifications',
    'queryString',
    'Admins'
];

angular.module('orca.admins')
    .controller('AdminsListCtrl', AdminsListCtrl);