function AdminsRoutes($routeProvider) {
    $routeProvider
        .when('/admins', {
            templateUrl: 'admins/list.html',
            controller: 'AdminsListCtrl',
            controllerAs: 'adminsListCtrl',
            label: 'Admins'
        });
}

AdminsRoutes.$inject = [
    '$routeProvider'
];

angular.module('orca.admins').config(AdminsRoutes);