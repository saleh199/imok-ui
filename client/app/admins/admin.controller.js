function AdminCtrl($scope, $rootScope, $uibModalInstance,
                   dgNotifications, Admins, itemId)
{
    $scope.map = null;
    $scope.id = itemId;
    $scope.dirtyData = {};
    $scope.cleanData = {};

    $scope.loading = [];

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.getAdminInfo = function()
    {
        var promise = Admins.get(
            { id: $scope.id },
            function(successResponse){
                $scope.cleanData = successResponse.result;
                $scope.dirtyData = angular.copy($scope.cleanData);
            }, function(errorResponse){
                dgNotifications.error(errorResponse);
                $scope.close();
            }
        );

        $scope.loading.push(promise);
    };

    $scope.setAdminInfo = function()
    {
        var data = angular.copy($scope.dirtyData),
            urlData = {},
            action = 'insert',
            loading = null;

        if($scope.id) {
            urlData.id = $scope.id;
            loading = Admins.update(
                urlData,
                data,
                function(successResponse){
                    dgNotifications.success('Admin Saved Successfully');
                    $rootScope.$broadcast('event:update');
                    $scope.close();
                }, function(errorResponse){
                    dgNotifications.error(errorResponse);
                }
            );
        }else{
            loading = Admins.insert(
                urlData,
                data,
                function(successResponse){
                    dgNotifications.success('Admin Saved Successfully');
                    $rootScope.$broadcast('event:update');
                    $scope.close();
                }, function(errorResponse){
                    dgNotifications.error(errorResponse);
                }
            );
        }

        $scope.loading.push(loading);
    };

    if($scope.id) {
        $scope.getAdminInfo();
    }
}

AdminCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$uibModalInstance',
    'dgNotifications',
    'Admins',
    'itemId'
];

angular.module('orca.admins')
    .controller('AdminCtrl', AdminCtrl);