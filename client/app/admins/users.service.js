function Admins(resource)
{
    var url = 'admins/:id';

    var resourceService = resource(
        url,
        {
            id: '@id'
        },
        {
            query: {
                isArray: false
            },
            update: {
                url: 'admins/:id',
                method: 'PUT'
            },
            insert: {
                url: 'admins',
                method: 'POST'
            }
        }
    );

    return resourceService;
}

Admins.$inject = [
    'resource'
];

angular.module('orca.admins')
    .factory('Admins', Admins);