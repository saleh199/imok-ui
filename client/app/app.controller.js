function AppCtrl($rootScope,  $window, $localStorage, $location,
                 dgNotifications, dgPopup, Auth, environmentConfig, Country)
{
    var self = this;

    self.windowResized = function(){
        if($window.innerWidth < 750) {
            $rootScope.screenWidth = 'xs';
        } else if($window.innerWidth < 970) {
            $rootScope.screenWidth = 'sm';
        } else if($window.innerWidth < 1170) {
            $rootScope.screenWidth = 'md';
        } else {
            $rootScope.screenWidth = 'lg';
        }
    };

    $rootScope.mapKey = 'AIzaSyDnPjbFn8NOQmdGn9aAQzknDkVWUBad8i0';
    $rootScope.screenWidth = '';

    angular.element($window).bind('resize', function(){
        self.windowResized();
        $rootScope.$apply();
    });

    $rootScope.heading = {};
    $rootScope.heading.name = 'Home Page';

    $rootScope.environment = environmentConfig;

    $rootScope.user = null;
    $rootScope.loadingBase = [];
    $rootScope.title = null;
    $rootScope.angular = angular;
    $rootScope.user = null;
    $rootScope.loadingBase = [];

    $rootScope.settings = {
        skin: 'assets/css/skins/purple.min.css',
        color: {
            themeprimary: '#2dc3e8',
            themesecondary: '#fb6e52',
            themethirdcolor: '#ffce55',
            themefourthcolor: '#a0d468',
            themefifthcolor: '#e75b8d'
        },
        rtl: true,
        fixed: {
            navbar: false,
            sidebar: false,
            breadcrumbs: false,
            header: false
        }
    };

    if (angular.isDefined($localStorage.settings))
        $rootScope.settings = $localStorage.settings;
    else
        $localStorage.settings = $rootScope.settings;

    $rootScope.$watch('settings', function() {
        if ($rootScope.settings.fixed.header) {
            $rootScope.settings.fixed.navbar = true;
            $rootScope.settings.fixed.sidebar = true;
            $rootScope.settings.fixed.breadcrumbs = true;
        }
        if ($rootScope.settings.fixed.breadcrumbs) {
            $rootScope.settings.fixed.navbar = true;
            $rootScope.settings.fixed.sidebar = true;
        }
        if ($rootScope.settings.fixed.sidebar)
            $rootScope.settings.fixed.navbar = true;

        $localStorage.settings = $rootScope.settings;
    }, true);

    $rootScope.openModal = function(name, controllerName, id, parentId, data, options) {
        data = data || {};
        options = options || {};

        var modalData = {
            templateUrl: name,
            controller: controllerName
        };

        if(id) {
            modalData.itemId = id;
        }

        if(parentId) {
            modalData.parentId = parentId;
        }

        if(angular.isObject(data)) {
            modalData.data = data;
        }

        if(angular.isObject(options)) {
            modalData.modalOptions = options;
        }

        var modalInstance = dgPopup.openModal(modalData);

        modalInstance.result.then(function(modalResponse) {
            if(modalResponse.type) {
                if(modalResponse.type === 'openModal') {
                    $rootScope.openModal(
                        modalResponse.data.templateUrl,
                        modalResponse.data.controller,
                        modalResponse.data.resolve.itemId
                    );
                }
            }
            return true;
        }, function () {
        });

        return modalInstance;
    };

    $rootScope.$on('$routeChangeStart', function($event, $route) {
        if($rootScope.screenWidth === 'xs' && $rootScope.adminId){
            angular.element('.page-sidebar').addClass('menu-compact').addClass('hide');
        }

        if($route && $route.templateUrl && $route.templateUrl.indexOf('modals') === -1) {
            $rootScope.heading.name = '';
            $rootScope.heading.total = null;
        }
    });

    $rootScope.getAdmin = function() {
        $rootScope.loadingAdmin = Auth.getAdmin()
            .then(function(successResponse){
                $rootScope.adminId = successResponse.data.result;
                $rootScope.$broadcast('admin.got', $rootScope.adminId);
            }, function(errorResponse){
                $rootScope.adminId = null;
                var redirectPage = $location.search().redirect || $location.url();
                $location.url('/')
                    .search({
                        redirect: redirectPage
                    });
            });

        $rootScope.loadingBase.push($rootScope.loadingAdmin);
    };

    $rootScope.logout = function() {
        $rootScope.loading = Auth.logout(
            function(successResponse) {
                $rootScope.adminId = null;
                $location.url('/');
            },
            function(errorResponse) {
                dgNotifications.error(errorResponse);
            }
        );
    };

    $rootScope.$on('admin.got', function(event, adminId) {
        if(adminId) {
            if(self.redirectTo) {
                $location.url(self.redirectTo)
                    .search('redirect', null);
            } else {
                //$location.url('/events');
            }
        }
    });

    $rootScope.getCountries = function(){
        return Country.getItems();
    };

    $rootScope.getCountry = function(id) {
        return Country.getItem(id);
    };

    $rootScope.getAdmin();
    self.windowResized();

    $rootScope.$on('admin.got', function(){
        Country.init();
    });
}

AppCtrl.$inject = [
    '$rootScope',
    '$window',
    '$localStorage',
    '$location',
    'dgNotifications',
    'dgPopup',
    'Auth',
    'environmentConfig',
    'Country'
];

angular
    .module('orca.imok')
    .controller('AppCtrl', AppCtrl);