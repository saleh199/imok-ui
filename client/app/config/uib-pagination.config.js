function UibPaginationConfig(uibPaginationConfig) {
    uibPaginationConfig.nextText = 'Next';
    uibPaginationConfig.previousText = 'Previous';
    uibPaginationConfig.firstText = 'First';
    uibPaginationConfig.lastText = 'Last';
}

function UibPagerConfig(uibPagerConfig) {
    uibPagerConfig.nextText = 'Next';
    uibPagerConfig.previousText = 'Previous';
}

UibPaginationConfig.$inject = [
    'uibPaginationConfig'
];
UibPagerConfig.$inject = [
    'uibPagerConfig'
];

angular.module('orca.imok')
    .config(UibPaginationConfig)
    .config(UibPagerConfig);