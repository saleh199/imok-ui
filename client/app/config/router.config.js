function RouterConfig($locationProvider) {
    $locationProvider.html5Mode(true);
}

RouterConfig.$inject = [
    '$locationProvider'
];

angular.module('orca.imok').config(RouterConfig);