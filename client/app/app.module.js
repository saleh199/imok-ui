angular.module('orca.imok', [
    // vendor
    'ngRoute',
    'ngSanitize',
    'ngAnimate',
    'ngStorage',
    'ngCookies',
    'ngResource',
    'ngTouch',
    'ui.bootstrap',
    'ui.select',
    'ui.utils',
    'ui.bootstrap.datepicker',
    'cgBusy',
    'ng-breadcrumbs',
    'daterangepicker',
    'beyond',
    'angular-cache', // for localStorage cache
    'toaster', // toaster notifications wrapper for angular
    'angularMoment', // momentjs wrapper for angular
    'dugun.popup', // modal opener
    'dugun.forms',
    'dugun.notifications',
    // App
    'orca.login',
    'orca.events',
    'orca.users',
    'orca.admins',
]);