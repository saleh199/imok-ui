function UsersListCtrl($scope, $rootScope, $route, dgNotifications, queryString, Users)
{
    $scope.loading = [];
    $scope.list = [];

    $scope.filters = queryString.getFilters() || {};

    $scope.loadList = function(){
        var data = angular.copy($scope.filters);

        var loading = Users.query(
            data,
            function(successResponse){
                $scope.list = successResponse.result;
            }, function(errorResponse){
                dgNotifications.error(errorResponse);
            }
        );

        $scope.loading.push(loading);
    };

    $scope.setFilters = function () {
        var data = angular.copy($scope.filters);
        queryString.setFilters(data);
        $scope.loadList();
    };

    $scope.resetFilters = function() {
        $scope.filters = {};
        $scope.setFilters();
    };


    $rootScope.$on('user:update', function(){
        $scope.loadList();
    });

    $scope.loadList();
    $scope.$root.heading.name = $route.current.label;
}

UsersListCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$route',
    'dgNotifications',
    'queryString',
    'Users'
];

angular.module('orca.users')
    .controller('UsersListCtrl', UsersListCtrl);