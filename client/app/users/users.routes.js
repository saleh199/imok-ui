function UsersRoutes($routeProvider) {
    $routeProvider
        .when('/users', {
            templateUrl: 'users/list.html',
            controller: 'UsersListCtrl',
            controllerAs: 'usersListCtrl',
            label: 'Users'
        });
}

UsersRoutes.$inject = [
    '$routeProvider'
];

angular.module('orca.users').config(UsersRoutes);