function Users(resource)
{
    var url = 'users/:id';

    var resourceService = resource(
        url,
        {
            id: '@id'
        },
        {
            query: {
                isArray: false
            },
            update: {
                url: 'users/:id',
                method: 'PUT'
            },
            insert: {
                url: 'users',
                method: 'POST'
            }
        }
    );

    return resourceService;
}

Users.$inject = [
    'resource'
];

angular.module('orca.users')
    .factory('Users', Users);