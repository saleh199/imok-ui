angular.module('orca.login')
    .controller('LoginCtrl', ['$scope', '$location', 'Auth', LoginCtrl]);

function LoginCtrl($scope, $location, Auth) {
    var self = this;

    $scope.alerts = [];
    self.redirectTo = ($location.search() || {}).redirect;

    $scope.login = function()
    {
        $scope.$broadcast('autofill:update');

        $scope.loading = Auth.login($scope.email, $scope.password)
            .then(function(adminId) {
                $scope.$root.adminId = adminId;
                $scope.$root.getAdmin();

                redirectToHome();
            }, function(response) {
                //dgNotifications.error(response);
            });

    };

    function redirectToHome() {
        if(self.redirectTo) {
            $location.url(self.redirectTo)
                .search('redirect', null);
        } else {
            $location.url('/events');
        }
    }
}