function LoginRoutes($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'login/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'loginCtrl',
            label: 'Login'
        });
}

LoginRoutes.$inject = [
    '$routeProvider'
];

angular.module('orca.imok').config(LoginRoutes);