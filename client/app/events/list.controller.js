function EventsListCtrl($scope, $rootScope, $route, dgNotifications, queryString, Events)
{
    $scope.loading = [];
    $scope.list = [];

    $scope.filters = queryString.getFilters() || {};

    $scope.loadList = function(){
        var data = angular.copy($scope.filters);

        var loading = Events.query(
            data,
            function(successResponse){
                $scope.list = successResponse.result;
            }, function(errorResponse){
                dgNotifications.error(errorResponse);
            }
        );

        $scope.loading.push(loading);
    };

    $scope.setFilters = function () {
        var data = angular.copy($scope.filters);
        queryString.setFilters(data);
        $scope.loadList();
    };

    $scope.resetFilters = function() {
        $scope.filters = {};
        $scope.setFilters();
    };

    $scope.openItemForm = function(item) {
        $scope.openModal(
            'events/form.modal.html',
            'EventCtrl',
            item ? item._id : null
        );
    };

    $scope.publishEvent = function(item){
        var loading = Events.publishEvent(
            { id: item._id },
            { is_published: true },
            function(successResponse){
                dgNotifications.success('Event was published successfully');
                $scope.loadList();
            },
            function(errorResponse){
                dgNotifications.error(errorResponse);
            }
        );

        $scope.loading.push(loading);
    };

    $rootScope.$on('event:update', function(){
        $scope.loadList();
    });

    $scope.loadList();
    $scope.$root.heading.name = $route.current.label;
}

EventsListCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$route',
    'dgNotifications',
    'queryString',
    'Events'
];

angular.module('orca.events')
    .controller('EventsListCtrl', EventsListCtrl);