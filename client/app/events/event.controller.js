function EventCtrl($scope, $rootScope, $window, $uibModalInstance,
                   NgMap, dgNotifications, Events, itemId)
{
    $scope.map = null;
    $scope.id = itemId;
    $scope.dirtyData = {
        location: 'current'
    };
    $scope.cleanData = {};

    $scope.loading = [];

    NgMap.getMap().then(function(evtMap) {
        $scope.map = evtMap;
        google.maps.event.trigger($scope.map, "resize");
    });

    $scope.close = function() {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.getEvent = function()
    {
        var promise = Events.get(
            { id: $scope.id },
            function(successResponse){
                $scope.cleanData = successResponse.result;
                $scope.dirtyData = angular.copy($scope.cleanData);

                var lng = $scope.dirtyData.location[0];
                $scope.dirtyData.location[0] = $scope.dirtyData.location[1];
                $scope.dirtyData.location[1] = lng;

            }, function(errorResponse){
                dgNotifications.error(errorResponse);
                $scope.close();
            }
        );

        $scope.loading.push(promise);
    };

    $scope.setEvent = function()
    {
        var data = angular.copy($scope.dirtyData),
            urlData = {},
            action = 'insert',
            loading = null;

        data.latitude = data.location[0];
        data.longitude = data.location[1];

        delete data.location;

        if($scope.id) {
            urlData.id = $scope.id;
            loading = Events.update(
                urlData,
                data,
                function(successResponse){
                    dgNotifications.success('Event Saved Successfully');
                    $rootScope.$broadcast('event:update');
                    $scope.close();
                }, function(errorResponse){
                    dgNotifications.error(errorResponse);
                }
            );
        }else{
            loading = Events.insert(
                urlData,
                data,
                function(successResponse){
                    dgNotifications.success('Event Saved Successfully');
                    $rootScope.$broadcast('event:update');
                    $scope.close();
                }, function(errorResponse){
                    dgNotifications.error(errorResponse);
                }
            );
        }



        $scope.loading.push(loading);
    };

    $scope.setMarkerPosition = function(event){
        $scope.dirtyData.location = [event.latLng.lat(), event.latLng.lng()];
    };

    $scope.$watch('dirtyData.countryId', function(value){
        if(value){
            var country = $rootScope.getCountry(value);
            //$scope.map.setCenter(new google.maps.LatLng(country.latlng[0], country.latlng[1]));
            $scope.dirtyData.location = country.latlng;
        }
    });

    if($scope.id) {
        $scope.getEvent();
    }
}

EventCtrl.$inject = [
    '$scope',
    '$rootScope',
    '$window',
    '$uibModalInstance',
    'NgMap',
    'dgNotifications',
    'Events',
    'itemId'
];

angular.module('orca.events')
    .controller('EventCtrl', EventCtrl);