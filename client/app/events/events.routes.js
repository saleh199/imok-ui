function EventsRoutes($routeProvider) {
    $routeProvider
        .when('/events', {
            templateUrl: 'events/list.html',
            controller: 'EventsListCtrl',
            controllerAs: 'eventsListCtrl',
            label: 'Events'
        });
}

EventsRoutes.$inject = [
    '$routeProvider'
];

angular.module('orca.events').config(EventsRoutes);