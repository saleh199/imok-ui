function Events(resource)
{
    var url = 'events/:id';

    var resourceService = resource(
        url,
        {
            id: '@id'
        },
        {
            query: {
                isArray: false
            },
            update: {
                url: 'events/:id',
                method: 'PUT'
            },
            insert: {
                url: 'events',
                method: 'POST'
            },
            publishEvent: {
                url: 'events/:id/publish',
                method: 'POST'
            }
        }
    );

    return resourceService;
}

Events.$inject = [
    'resource'
];

angular.module('orca.events')
    .factory('Events', Events);